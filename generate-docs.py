import time
import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class ChangeHandler(FileSystemEventHandler):
    def on_any_event(self, event):
        # Triggered on any file change in 'schemas' directory
        print(f'Event type: {event.event_type}  path : {event.src_path}')
        self.cleanup_docs()
        self.run_jsonschema2md()

    def on_modified(self, event):
        print(f'event type: {event.event_type}  path : {event.src_path}')

    @staticmethod
    def cleanup_docs():
        print("Cleaning up 'docs' directory...")
        subprocess.run(["rm", "-rf", "./docs/*"])  # Be cautious with rm -rf

    @staticmethod
    def run_jsonschema2md():
        command = ["jsonschema2md", "-d", "./schemas", "-o", "./docs", "-x", "-", "-e", "json"]

        print("Running jsonschema2md...")
        print(" ".join(command))
        subprocess.run(command)

def main():
    path = "./schemas"
    event_handler = ChangeHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    print(f"Observer started. Monitoring {path} for changes...")
    try:
        while True:
            event_handler.cleanup_docs()
            event_handler.run_jsonschema2md()
            time.sleep(60)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

if __name__ == "__main__":
    main()
