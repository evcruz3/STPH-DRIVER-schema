# Untitled string in Vehicle Schema

```txt
Vehicle#/definitions/LoadingEnum
```

The loading status of a vehicle.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## LoadingEnum Type

`string`

## LoadingEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value           | Explanation |
| :-------------- | :---------- |
| `"Legal"`       |             |
| `"Overloaded"`  |             |
| `"Unsafe Load"` |             |
