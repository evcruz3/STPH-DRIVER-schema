# Untitled string in Incident Details Schema

```txt
IncidentDetails#/definitions/SeverityEnum
```

The severity levels of the incident.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## SeverityEnum Type

`string`

## SeverityEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value               | Explanation |
| :------------------ | :---------- |
| `"Property Damage"` |             |
| `"Injury"`          |             |
| `"Fatal"`           |             |
