# Untitled string in Person Schema

```txt
Person#/definitions/WearStateEnum
```

Describes the correctness of safety gear usage.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                     |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Person.json\*](../schemas/Person.json "open original schema") |

## WearStateEnum Type

`string`

## WearStateEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Worn Correctly"`     |             |
| `"Not Worn"`           |             |
| `"Not Worn Correctly"` |             |
