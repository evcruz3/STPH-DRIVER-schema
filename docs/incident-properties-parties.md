# Untitled array in Incident Schema

```txt
Parties#/properties/parties
```

A list of parties involved in the incident

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                         |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Incident.json\*](../schemas/Incident.json "open original schema") |

## parties Type

`object[]` ([Party](party.md))
