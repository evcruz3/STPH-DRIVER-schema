# README

## Top-level Schemas

*   [Adult](./adult.md "An adult involved in an incident, detailing their personal information, involvement, and condition") – `Adult`

*   [Crash Diagram](./crashdiagram.md) – `CrashDiagram`

*   [Incident](./schema.md "A record of a road incident") – `Incident`

*   [Incident Details](./incidentdetails.md "A schema representing the details of an incident, including location, time, environmental conditions, and other relevant information") – `IncidentDetails`

*   [Minor](./minor.md "A child or minor involved in an incident, detailing their personal information, involvement, and condition") – `Minor`

*   [Party](./party.md "A representation of a party including adults, minors, a vehicle, and photos") – `Party`

*   [Person](./person.md "A schema representing a person involved in an incident, detailing their personal information, involvement, and condition") – `Person`

*   [Photo](./photo.md "Picture of an incident") – `Photo`

*   [Vehicle](./vehicle.md) – `Vehicle`

## Other Schemas

### Objects

*   [Untitled object in Incident Details](./incidentdetails-definitions-location.md "Defines the geographical location of the incident") – `IncidentDetails#/definitions/Location`

### Arrays

*   [Untitled array in Incident](./schema-properties-parties.md "A list of parties involved in the incident") – `Parties#/properties/parties`

*   [Untitled array in Incident Details](./incidentdetails-properties-severity.md "The severity level(s) of the incident") – `IncidentDetails#/properties/severity`

*   [Untitled array in Party](./party-properties-adults.md "An array of adults in a party") – `Party#/properties/adults`

*   [Untitled array in Party](./party-properties-minors.md "An array of minors in a party") – `Party#/properties/minors`

*   [Untitled array in Party](./party-properties-photos.md "An array of photos related to the party") – `Party#/properties/photos`

*   [Untitled array in Vehicle](./vehicle-properties-damages.md "The damages to the vehicle") – `Vehicle#/properties/damages`

*   [Untitled array in Vehicle](./vehicle-properties-defects.md "Any defects the vehicle has") – `Vehicle#/properties/defects`

## Version Note

The schemas linked above follow the JSON Schema Spec version: `http://json-schema.org/draft-07/schema#`
