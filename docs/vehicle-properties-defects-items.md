# Untitled string in Vehicle Schema

```txt
Vehicle#/properties/defects/items
```

The types of defects a vehicle can have.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## items Type

`string`

## items Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value        | Explanation |
| :----------- | :---------- |
| `"Lights"`   |             |
| `"Brakes"`   |             |
| `"Steering"` |             |
| `"Tires"`    |             |
