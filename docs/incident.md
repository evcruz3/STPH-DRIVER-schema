# Incident Schema

```txt
Incident
```

A record of a road incident

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [Incident.json](../schemas/Incident.json "open original schema") |

## Incident Type

`object` ([Incident](incident.md))

# Incident Properties

| Property                               | Type     | Required | Nullable       | Defined by                                                                    |
| :------------------------------------- | :------- | :------- | :------------- | :---------------------------------------------------------------------------- |
| [incident\_details](#incident_details) | `object` | Optional | cannot be null | [Incident](incidentdetails.md "IncidentDetails#/properties/incident_details") |
| [parties](#parties)                    | `array`  | Optional | cannot be null | [Incident](incident-properties-parties.md "Parties#/properties/parties")      |
| [notes](#notes)                        | `string` | Optional | cannot be null | [Incident](incident-properties-notes.md "Incident#/properties/notes")         |
| [crash\_diagram](#crash_diagram)       | `object` | Optional | cannot be null | [Incident](crashdiagram.md "CrashDiagram#/properties/crash_diagram")          |

## incident\_details

A schema representing the details of an incident, including location, time, environmental conditions, and other relevant information.

`incident_details`

*   is optional

*   Type: `object` ([Incident Details](incidentdetails.md))

*   cannot be null

*   defined in: [Incident](incidentdetails.md "IncidentDetails#/properties/incident_details")

### incident\_details Type

`object` ([Incident Details](incidentdetails.md))

## parties

A list of parties involved in the incident

`parties`

*   is optional

*   Type: `object[]` ([Party](party.md))

*   cannot be null

*   defined in: [Incident](incident-properties-parties.md "Parties#/properties/parties")

### parties Type

`object[]` ([Party](party.md))

## notes



`notes`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident](incident-properties-notes.md "Incident#/properties/notes")

### notes Type

`string`

## crash\_diagram



`crash_diagram`

*   is optional

*   Type: `object` ([Crash Diagram](crashdiagram.md))

*   cannot be null

*   defined in: [Incident](crashdiagram.md "CrashDiagram#/properties/crash_diagram")

### crash\_diagram Type

`object` ([Crash Diagram](crashdiagram.md))
