# Photo Schema

```txt
Photo
```

Picture of an incident

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                 |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [Photo.json](../schemas/Photo.json "open original schema") |

## Photo Type

`object` ([Photo](photo.md))

# Photo Properties

| Property                    | Type     | Required | Nullable       | Defined by                                                               |
| :-------------------------- | :------- | :------- | :------------- | :----------------------------------------------------------------------- |
| [\_localId](#_localid)      | `string` | Optional | cannot be null | [Photo](photo-properties-_localid.md "Photo#/properties/_localId")       |
| [picture](#picture)         | `string` | Required | cannot be null | [Photo](photo-properties-picture.md "Photo#/properties/picture")         |
| [description](#description) | `string` | Required | cannot be null | [Photo](photo-properties-description.md "Photo#/properties/description") |

## \_localId

The local identifier of the photo, used to reference the photo in the incident report.

`_localId`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Photo](photo-properties-_localid.md "Photo#/properties/_localId")

### \_localId Type

`string`

## picture

A base64 encoded string of the photo, expected to be in JPEG format.

`picture`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Photo](photo-properties-picture.md "Photo#/properties/picture")

### picture Type

`string`

## description

A textual description of the photo.

`description`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Photo](photo-properties-description.md "Photo#/properties/description")

### description Type

`string`
