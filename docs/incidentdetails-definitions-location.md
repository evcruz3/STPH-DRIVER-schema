# Untitled object in Incident Details Schema

```txt
IncidentDetails#/definitions/Location
```

Defines the geographical location of the incident.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## Location Type

`object` ([Details](incidentdetails-definitions-location.md))

# Location Properties

| Property                         | Type     | Required | Nullable       | Defined by                                                                                                                                            |
| :------------------------------- | :------- | :------- | :------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------- |
| [latitude](#latitude)            | `number` | Required | cannot be null | [Incident Details](incidentdetails-definitions-location-properties-latitude.md "IncidentDetails#/definitions/Location/properties/latitude")           |
| [longitude](#longitude)          | `number` | Required | cannot be null | [Incident Details](incidentdetails-definitions-location-properties-longitude.md "IncidentDetails#/definitions/Location/properties/longitude")         |
| [location\_name](#location_name) | `string` | Optional | cannot be null | [Incident Details](incidentdetails-definitions-location-properties-location_name.md "IncidentDetails#/definitions/Location/properties/location_name") |

## latitude

The latitude of the location.

`latitude`

*   is required

*   Type: `number`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location-properties-latitude.md "IncidentDetails#/definitions/Location/properties/latitude")

### latitude Type

`number`

### latitude Constraints

**unknown format**: the value of this string must follow the format: `float`

## longitude

The longitude of the location.

`longitude`

*   is required

*   Type: `number`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location-properties-longitude.md "IncidentDetails#/definitions/Location/properties/longitude")

### longitude Type

`number`

### longitude Constraints

**unknown format**: the value of this string must follow the format: `float`

## location\_name

The name of the location.

`location_name`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location-properties-location_name.md "IncidentDetails#/definitions/Location/properties/location_name")

### location\_name Type

`string`
