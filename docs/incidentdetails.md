# Incident Details Schema

```txt
IncidentDetails
```

A schema representing the details of an incident, including location, time, environmental conditions, and other relevant information.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                     |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------------------------- |
| Can be instantiated | Yes        | Unknown status | No           | Forbidden         | Allowed               | none                | [IncidentDetails.json](../schemas/IncidentDetails.json "open original schema") |

## Incident Details Type

`object` ([Incident Details](incidentdetails.md))

# Incident Details Properties

| Property                                       | Type      | Required | Nullable       | Defined by                                                                                                                |
| :--------------------------------------------- | :-------- | :------- | :------------- | :------------------------------------------------------------------------------------------------------------------------ |
| [location](#location)                          | `object`  | Optional | cannot be null | [Incident Details](incidentdetails-definitions-location.md "IncidentDetails#/properties/location")                        |
| [date\_time](#date_time)                       | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-date_time.md "IncidentDetails#/properties/date_time")                       |
| [weather](#weather)                            | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-weather.md "IncidentDetails#/properties/weather")                           |
| [light](#light)                                | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-light.md "IncidentDetails#/properties/light")                               |
| [severity](#severity)                          | `array`   | Optional | cannot be null | [Incident Details](incidentdetails-properties-severity.md "IncidentDetails#/properties/severity")                         |
| [main\_cause](#main_cause)                     | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-main_cause.md "IncidentDetails#/properties/main_cause")                     |
| [collision\_type](#collision_type)             | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-collision_type.md "IncidentDetails#/properties/collision_type")             |
| [reporting\_agency](#reporting_agency)         | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-reporting_agency.md "IncidentDetails#/properties/reporting_agency")         |
| [encoder\_email](#encoder_email)               | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-encoder_email.md "IncidentDetails#/properties/encoder_email")               |
| [description](#description)                    | `string`  | Optional | cannot be null | [Incident Details](incidentdetails-properties-description.md "IncidentDetails#/properties/description")                   |
| [location\_approximate](#location_approximate) | `boolean` | Optional | cannot be null | [Incident Details](incidentdetails-properties-location_approximate.md "IncidentDetails#/properties/location_approximate") |

## location

Defines the geographical location of the incident.

`location`

*   is optional

*   Type: `object` ([Details](incidentdetails-definitions-location.md))

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location.md "IncidentDetails#/properties/location")

### location Type

`object` ([Details](incidentdetails-definitions-location.md))

## date\_time

The date and time when the incident occurred.

`date_time`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-date_time.md "IncidentDetails#/properties/date_time")

### date\_time Type

`string`

### date\_time Constraints

**date time**: the string must be a date time string, according to [RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339 "check the specification")

## weather

The types of weather conditions at the time of the incident.

`weather`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-weather.md "IncidentDetails#/properties/weather")

### weather Type

`string`

### weather Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                      | Explanation |
| :------------------------- | :---------- |
| `"Day"`                    |             |
| `"Clear Night"`            |             |
| `"Cloudy"`                 |             |
| `"Fog"`                    |             |
| `"Hail"`                   |             |
| `"Partially Cloudy Day"`   |             |
| `"Partially Cloudy Night"` |             |
| `"Rain"`                   |             |
| `"Thunderstorm"`           |             |
| `"Wind"`                   |             |

## light

The light conditions during the incident.

`light`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-light.md "IncidentDetails#/properties/light")

### light Type

`string`

### light Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value     | Explanation |
| :-------- | :---------- |
| `"Dawn"`  |             |
| `"Day"`   |             |
| `"Dusk"`  |             |
| `"Night"` |             |

## severity

The severity level(s) of the incident.

`severity`

*   is optional

*   Type: `string[]`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-severity.md "IncidentDetails#/properties/severity")

### severity Type

`string[]`

## main\_cause

The main causes for the incident.

`main_cause`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-main_cause.md "IncidentDetails#/properties/main_cause")

### main\_cause Type

`string`

### main\_cause Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value              | Explanation |
| :----------------- | :---------- |
| `"Human Error"`    |             |
| `"Vehicle Defect"` |             |
| `"Road Defect"`    |             |
| `"Other"`          |             |

## collision\_type

Different types of collisions that can occur.

`collision_type`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-collision_type.md "IncidentDetails#/properties/collision_type")

### collision\_type Type

`string`

### collision\_type Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                   | Explanation |
| :---------------------- | :---------- |
| `"Head On"`             |             |
| `"Rear End"`            |             |
| `"Right Angle"`         |             |
| `"Other Angle"`         |             |
| `"Side Swipe"`          |             |
| `"Overturned Vehicle"`  |             |
| `"Hit Object On Road"`  |             |
| `"Hit Object Off Road"` |             |
| `"Hit Parked Vehicle"`  |             |
| `"Hit Pedestrian"`      |             |
| `"Hit Animal"`          |             |
| `"Other"`               |             |

## reporting\_agency

The agencies that can report an incident.

`reporting_agency`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-reporting_agency.md "IncidentDetails#/properties/reporting_agency")

### reporting\_agency Type

`string`

### reporting\_agency Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"PNP"`                |             |
| `"Local Traffic Unit"` |             |
| `"BGCEA"`              |             |
| `"CCTO"`               |             |
| `"MMDA Metrobase"`     |             |
| `"Other"`              |             |

## encoder\_email

The email address of the person encoding the incident details.

`encoder_email`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-encoder_email.md "IncidentDetails#/properties/encoder_email")

### encoder\_email Type

`string`

### encoder\_email Constraints

**email**: the string must be an email address, according to [RFC 5322, section 3.4.1](https://tools.ietf.org/html/rfc5322 "check the specification")

## description

A textual description of the incident.

`description`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-description.md "IncidentDetails#/properties/description")

### description Type

`string`

## location\_approximate

Indicates whether the location details are approximate.

`location_approximate`

*   is optional

*   Type: `boolean`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-properties-location_approximate.md "IncidentDetails#/properties/location_approximate")

### location\_approximate Type

`boolean`

# Incident Details Definitions

## Definitions group Location

Reference this group by using

```json
{"$ref":"IncidentDetails#/definitions/Location"}
```

| Property                         | Type     | Required | Nullable       | Defined by                                                                                                                                            |
| :------------------------------- | :------- | :------- | :------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------- |
| [latitude](#latitude)            | `number` | Required | cannot be null | [Incident Details](incidentdetails-definitions-location-properties-latitude.md "IncidentDetails#/definitions/Location/properties/latitude")           |
| [longitude](#longitude)          | `number` | Required | cannot be null | [Incident Details](incidentdetails-definitions-location-properties-longitude.md "IncidentDetails#/definitions/Location/properties/longitude")         |
| [location\_name](#location_name) | `string` | Optional | cannot be null | [Incident Details](incidentdetails-definitions-location-properties-location_name.md "IncidentDetails#/definitions/Location/properties/location_name") |

### latitude

The latitude of the location.

`latitude`

*   is required

*   Type: `number`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location-properties-latitude.md "IncidentDetails#/definitions/Location/properties/latitude")

#### latitude Type

`number`

#### latitude Constraints

**unknown format**: the value of this string must follow the format: `float`

### longitude

The longitude of the location.

`longitude`

*   is required

*   Type: `number`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location-properties-longitude.md "IncidentDetails#/definitions/Location/properties/longitude")

#### longitude Type

`number`

#### longitude Constraints

**unknown format**: the value of this string must follow the format: `float`

### location\_name

The name of the location.

`location_name`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Incident Details](incidentdetails-definitions-location-properties-location_name.md "IncidentDetails#/definitions/Location/properties/location_name")

#### location\_name Type

`string`

## Definitions group SeverityEnum

Reference this group by using

```json
{"$ref":"SeverityEnum#/definitions/SeverityEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group LightEnum

Reference this group by using

```json
{"$ref":"IncidentDetails#/definitions/LightEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group WeatherEnum

Reference this group by using

```json
{"$ref":"IncidentDetails#/definitions/WeatherEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group AgencyEnum

Reference this group by using

```json
{"$ref":"IncidentDetails#/definitions/AgencyEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group CollisionTypeEnum

Reference this group by using

```json
{"$ref":"IncidentDetails#/definitions/CollisionTypeEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group MainCauseEnum

Reference this group by using

```json
{"$ref":"IncidentDetails#/definitions/MainCauseEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |
