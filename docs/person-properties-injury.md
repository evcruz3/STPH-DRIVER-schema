# Untitled string in Person Schema

```txt
Person#/properties/injury
```

Categorizes the severity of injuries.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                     |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Person.json\*](../schemas/Person.json "open original schema") |

## injury Type

`string`

## injury Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value           | Explanation |
| :-------------- | :---------- |
| `"Fatal"`       |             |
| `"Serious"`     |             |
| `"Minor"`       |             |
| `"Not Injured"` |             |
