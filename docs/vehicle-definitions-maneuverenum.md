# Untitled string in Vehicle Schema

```txt
Vehicle#/definitions/ManeuverEnum
```

Possible maneuvers a vehicle can perform.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## ManeuverEnum Type

`string`

## ManeuverEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value               | Explanation |
| :------------------ | :---------- |
| `"Left Turn"`       |             |
| `"Right Turn"`      |             |
| `"U Turn"`          |             |
| `"Cross Traffic"`   |             |
| `"Merging"`         |             |
| `"Diverging"`       |             |
| `"Overtaking"`      |             |
| `"Going Ahead"`     |             |
| `"Reversing"`       |             |
| `"Sudden Start"`    |             |
| `"Sudden Stop"`     |             |
| `"Parked Off Road"` |             |
| `"Parked On Road"`  |             |
