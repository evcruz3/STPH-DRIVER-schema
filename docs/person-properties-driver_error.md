# Untitled string in Person Schema

```txt
Person#/properties/driver_error
```

Lists possible driver errors contributing to incidents.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                     |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Person.json\*](../schemas/Person.json "open original schema") |

## driver\_error Type

`string`

## driver\_error Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Fatigued Or Asleep"` |             |
| `"Inattentive"`        |             |
| `"Too Fast"`           |             |
| `"Too Close"`          |             |
| `"No Signal"`          |             |
| `"Bad Overtaking"`     |             |
| `"Bad Turning"`        |             |
| `"Using Cellphone"`    |             |
