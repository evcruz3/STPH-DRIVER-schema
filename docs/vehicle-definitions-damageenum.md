# Untitled string in Vehicle Schema

```txt
Vehicle#/definitions/DamageEnum
```

The areas where a vehicle can be damaged.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## DamageEnum Type

`string`

## DamageEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value     | Explanation |
| :-------- | :---------- |
| `"Front"` |             |
| `"Rear"`  |             |
| `"Right"` |             |
| `"Left"`  |             |
| `"Roof"`  |             |
