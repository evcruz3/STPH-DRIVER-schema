# Untitled string in Photo Schema

```txt
Photo#/properties/_localId
```

The local identifier of the photo, used to reference the photo in the incident report.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Photo.json\*](../schemas/Photo.json "open original schema") |

## \_localId Type

`string`
