# Untitled string in Crash Diagram Schema

```txt
CrashDiagram#/properties/crash_type
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                 |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [CrashDiagram.json\*](../schemas/CrashDiagram.json "open original schema") |

## crash\_type Type

`string`

## crash\_type Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                                                 | Explanation |
| :---------------------------------------------------- | :---------- |
| `"Pedestrian On Foot"`                                |             |
| `"Vehicle From Adjacent Direction Intersection Only"` |             |
| `"Vehicle From Adjacent Direction"`                   |             |
| `"Vehicle From Opposing Direction"`                   |             |
| `"Vehicle From Same Direction"`                       |             |
| `"Maneuvering"`                                       |             |
| `"Overtaking"`                                        |             |
| `"On Path"`                                           |             |
| `"Off Path Straight"`                                 |             |
| `"Off Path Curve"`                                    |             |
| `"Passenger And Miscellaneous"`                       |             |
