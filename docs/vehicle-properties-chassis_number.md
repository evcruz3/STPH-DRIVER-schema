# Untitled string in Vehicle Schema

```txt
Vehicle#/properties/chassis_number
```

The chassis number of the vehicle.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## chassis\_number Type

`string`
