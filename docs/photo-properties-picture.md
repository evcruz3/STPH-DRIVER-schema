# Untitled string in Photo Schema

```txt
Photo#/properties/picture
```

A base64 encoded string of the photo, expected to be in JPEG format.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Photo.json\*](../schemas/Photo.json "open original schema") |

## picture Type

`string`
