# Untitled undefined type in Minor Schema

```txt
Minor#/allOf/1
```



| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Allowed               | none                | [Minor.json\*](../schemas/Minor.json "open original schema") |

## 1 Type

unknown

# 1 Properties

| Property                     | Type     | Required | Nullable       | Defined by                                                                               |
| :--------------------------- | :------- | :------- | :------------- | :--------------------------------------------------------------------------------------- |
| [grade\_level](#grade_level) | `string` | Optional | cannot be null | [Minor](minor-allof-1-properties-grade_level.md "Minor#/allOf/1/properties/grade_level") |

## grade\_level

The grade level of the minor.

`grade_level`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Minor](minor-allof-1-properties-grade_level.md "Minor#/allOf/1/properties/grade_level")

### grade\_level Type

`string`

### grade\_level Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value           | Explanation |
| :-------------- | :---------- |
| `"Preschool"`   |             |
| `"Elementary"`  |             |
| `"High School"` |             |
