# Untitled string in Minor Schema

```txt
Minor#/allOf/1/properties/grade_level
```

The grade level of the minor.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Minor.json\*](../schemas/Minor.json "open original schema") |

## grade\_level Type

`string`

## grade\_level Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value           | Explanation |
| :-------------- | :---------- |
| `"Preschool"`   |             |
| `"Elementary"`  |             |
| `"High School"` |             |
