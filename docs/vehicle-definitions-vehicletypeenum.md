# Untitled string in Vehicle Schema

```txt
Vehicle#/definitions/VehicleTypeEnum
```

The types of vehicles.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## VehicleTypeEnum Type

`string`

## VehicleTypeEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                 | Explanation       |
| :-------------------- | :---------------- |
| `"Car"`               | Car               |
| `"Van"`               | Van               |
| `"Suv"`               | Suv               |
| `"Bus"`               | Bus               |
| `"Jeepney"`           | Jeepney           |
| `"Taxi Metered"`      | Taxi Metered      |
| `"Truck Pick Up"`     | Truck Pick Up     |
| `"Truck Rigid"`       | Truck Rigid       |
| `"Truck Articulated"` | Truck Articulated |
| `"Truck Fire"`        | Truck Fire        |
| `"Truck"`             | Truck             |
| `"Ambulance"`         | Ambulance         |
| `"Armored Car"`       | Armored Car       |
| `"Heavy Equipment"`   | Heavy Equipment   |
| `"Motorcycle"`        | Motorcycle        |
| `"Tricycle"`          | Tricycle          |
| `"Bicycle"`           | Bicycle           |
| `"Pedicab"`           | Pedicab           |
| `"Pedestrian"`        | Pedestrian        |
| `"Push Cart"`         | Push Cart         |
| `"Tartanilla"`        | Tartanilla        |
| `"Animal"`            | Animal            |
| `"Water Vessel"`      | Water Vessel      |
| `"Electric Bike"`     | Electric Bike     |
| `"Habal Habal"`       | Habal Habal       |
| `"Others"`            | Others            |
