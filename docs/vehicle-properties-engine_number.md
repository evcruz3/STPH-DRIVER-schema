# Untitled string in Vehicle Schema

```txt
Vehicle#/properties/engine_number
```

The engine number of the vehicle.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## engine\_number Type

`string`
