# Untitled string in Incident Details Schema

```txt
IncidentDetails#/properties/encoder_email
```

The email address of the person encoding the incident details.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## encoder\_email Type

`string`

## encoder\_email Constraints

**email**: the string must be an email address, according to [RFC 5322, section 3.4.1](https://tools.ietf.org/html/rfc5322 "check the specification")
