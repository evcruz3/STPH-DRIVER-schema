# Untitled string in Photo Schema

```txt
Photo#/properties/description
```

A textual description of the photo.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Photo.json\*](../schemas/Photo.json "open original schema") |

## description Type

`string`
