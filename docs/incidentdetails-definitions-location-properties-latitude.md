# Untitled number in Incident Details Schema

```txt
IncidentDetails#/definitions/Location/properties/latitude
```

The latitude of the location.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## latitude Type

`number`

## latitude Constraints

**unknown format**: the value of this string must follow the format: `float`
