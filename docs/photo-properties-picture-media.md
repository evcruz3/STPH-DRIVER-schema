# Untitled image/jpeg in Photo Schema

```txt
Photo#/properties/picture/media
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Photo.json\*](../schemas/Photo.json "open original schema") |

## media Type

`image/jpeg`
