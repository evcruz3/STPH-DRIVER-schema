# Untitled undefined type in Crash Diagram Schema

```txt
CrashDiagram#/definitions
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                 |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [CrashDiagram.json\*](../schemas/CrashDiagram.json "open original schema") |

## definitions Type

unknown
