# Untitled string in Party Schema

```txt
Party#/properties/name
```

A label or name for the party involved in the incident

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Party.json\*](../schemas/Party.json "open original schema") |

## name Type

`string`
