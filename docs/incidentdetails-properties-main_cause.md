# Untitled string in Incident Details Schema

```txt
IncidentDetails#/properties/main_cause
```

The main causes for the incident.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## main\_cause Type

`string`

## main\_cause Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value              | Explanation |
| :----------------- | :---------- |
| `"Human Error"`    |             |
| `"Vehicle Defect"` |             |
| `"Road Defect"`    |             |
| `"Other"`          |             |
