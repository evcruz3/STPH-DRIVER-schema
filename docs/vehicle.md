# Vehicle Schema

```txt
Vehicle
```



| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                     |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------- |
| Can be instantiated | Yes        | Unknown status | No           | Forbidden         | Allowed               | none                | [Vehicle.json](../schemas/Vehicle.json "open original schema") |

## Vehicle Type

`object` ([Vehicle](vehicle.md))

# Vehicle Properties

| Property                                 | Type     | Required | Nullable       | Defined by                                                                                       |
| :--------------------------------------- | :------- | :------- | :------------- | :----------------------------------------------------------------------------------------------- |
| [classification](#classification)        | `string` | Required | cannot be null | [Vehicle](vehicle-definitions-vehicleclassificationenum.md "Vehicle#/properties/classification") |
| [vehicle\_type](#vehicle_type)           | `string` | Required | cannot be null | [Vehicle](vehicle-definitions-vehicletypeenum.md "Vehicle#/properties/vehicle_type")             |
| [make](#make)                            | `string` | Required | cannot be null | [Vehicle](vehicle-properties-make.md "Vehicle#/properties/make")                                 |
| [plate\_number](#plate_number)           | `string` | Required | cannot be null | [Vehicle](vehicle-properties-plate_number.md "Vehicle#/properties/plate_number")                 |
| [model](#model)                          | `string` | Required | cannot be null | [Vehicle](vehicle-properties-model.md "Vehicle#/properties/model")                               |
| [maneuver](#maneuver)                    | `string` | Required | cannot be null | [Vehicle](vehicle-definitions-maneuverenum.md "Vehicle#/properties/maneuver")                    |
| [damages](#damages)                      | `array`  | Optional | cannot be null | [Vehicle](vehicle-properties-damages.md "Vehicle#/properties/damages")                           |
| [defects](#defects)                      | `array`  | Optional | cannot be null | [Vehicle](vehicle-properties-defects.md "Vehicle#/properties/defects")                           |
| [loading](#loading)                      | `string` | Required | cannot be null | [Vehicle](vehicle-definitions-loadingenum.md "Vehicle#/properties/loading")                      |
| [insurance\_details](#insurance_details) | `string` | Required | cannot be null | [Vehicle](vehicle-properties-insurance_details.md "Vehicle#/properties/insurance_details")       |
| [engine\_number](#engine_number)         | `string` | Required | cannot be null | [Vehicle](vehicle-properties-engine_number.md "Vehicle#/properties/engine_number")               |
| [chassis\_number](#chassis_number)       | `string` | Required | cannot be null | [Vehicle](vehicle-properties-chassis_number.md "Vehicle#/properties/chassis_number")             |

## classification

The classification categories for vehicles.

`classification`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-definitions-vehicleclassificationenum.md "Vehicle#/properties/classification")

### classification Type

`string`

### classification Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Private"`            |             |
| `"Government"`         |             |
| `"Public Or For Hire"` |             |
| `"Diplomat"`           |             |

## vehicle\_type

The types of vehicles.

`vehicle_type`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-definitions-vehicletypeenum.md "Vehicle#/properties/vehicle_type")

### vehicle\_type Type

`string`

### vehicle\_type Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                 | Explanation       |
| :-------------------- | :---------------- |
| `"Car"`               | Car               |
| `"Van"`               | Van               |
| `"Suv"`               | Suv               |
| `"Bus"`               | Bus               |
| `"Jeepney"`           | Jeepney           |
| `"Taxi Metered"`      | Taxi Metered      |
| `"Truck Pick Up"`     | Truck Pick Up     |
| `"Truck Rigid"`       | Truck Rigid       |
| `"Truck Articulated"` | Truck Articulated |
| `"Truck Fire"`        | Truck Fire        |
| `"Truck"`             | Truck             |
| `"Ambulance"`         | Ambulance         |
| `"Armored Car"`       | Armored Car       |
| `"Heavy Equipment"`   | Heavy Equipment   |
| `"Motorcycle"`        | Motorcycle        |
| `"Tricycle"`          | Tricycle          |
| `"Bicycle"`           | Bicycle           |
| `"Pedicab"`           | Pedicab           |
| `"Pedestrian"`        | Pedestrian        |
| `"Push Cart"`         | Push Cart         |
| `"Tartanilla"`        | Tartanilla        |
| `"Animal"`            | Animal            |
| `"Water Vessel"`      | Water Vessel      |
| `"Electric Bike"`     | Electric Bike     |
| `"Habal Habal"`       | Habal Habal       |
| `"Others"`            | Others            |

## make

The make of the vehicle.

`make`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-make.md "Vehicle#/properties/make")

### make Type

`string`

## plate\_number

The plate number of the vehicle.

`plate_number`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-plate_number.md "Vehicle#/properties/plate_number")

### plate\_number Type

`string`

## model

The model of the vehicle.

`model`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-model.md "Vehicle#/properties/model")

### model Type

`string`

## maneuver

Possible maneuvers a vehicle can perform.

`maneuver`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-definitions-maneuverenum.md "Vehicle#/properties/maneuver")

### maneuver Type

`string`

### maneuver Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value               | Explanation |
| :------------------ | :---------- |
| `"Left Turn"`       |             |
| `"Right Turn"`      |             |
| `"U Turn"`          |             |
| `"Cross Traffic"`   |             |
| `"Merging"`         |             |
| `"Diverging"`       |             |
| `"Overtaking"`      |             |
| `"Going Ahead"`     |             |
| `"Reversing"`       |             |
| `"Sudden Start"`    |             |
| `"Sudden Stop"`     |             |
| `"Parked Off Road"` |             |
| `"Parked On Road"`  |             |

## damages

The damages to the vehicle.

`damages`

*   is optional

*   Type: `string[]`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-damages.md "Vehicle#/properties/damages")

### damages Type

`string[]`

## defects

Any defects the vehicle has.

`defects`

*   is optional

*   Type: `string[]`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-defects.md "Vehicle#/properties/defects")

### defects Type

`string[]`

## loading

The loading status of a vehicle.

`loading`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-definitions-loadingenum.md "Vehicle#/properties/loading")

### loading Type

`string`

### loading Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value           | Explanation |
| :-------------- | :---------- |
| `"Legal"`       |             |
| `"Overloaded"`  |             |
| `"Unsafe Load"` |             |

## insurance\_details

The insurance details of the vehicle.

`insurance_details`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-insurance_details.md "Vehicle#/properties/insurance_details")

### insurance\_details Type

`string`

## engine\_number

The engine number of the vehicle.

`engine_number`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-engine_number.md "Vehicle#/properties/engine_number")

### engine\_number Type

`string`

## chassis\_number

The chassis number of the vehicle.

`chassis_number`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Vehicle](vehicle-properties-chassis_number.md "Vehicle#/properties/chassis_number")

### chassis\_number Type

`string`

# Vehicle Definitions

## Definitions group VehicleClassificationEnum

Reference this group by using

```json
{"$ref":"Vehicle#/definitions/VehicleClassificationEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group VehicleTypeEnum

Reference this group by using

```json
{"$ref":"Vehicle#/definitions/VehicleTypeEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group ManeuverEnum

Reference this group by using

```json
{"$ref":"Vehicle#/definitions/ManeuverEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group DamageEnum

Reference this group by using

```json
{"$ref":"Vehicle#/definitions/DamageEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group DefectEnum

Reference this group by using

```json
{"$ref":"Vehicle#/definitions/DefectEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group LoadingEnum

Reference this group by using

```json
{"$ref":"Vehicle#/definitions/LoadingEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |
