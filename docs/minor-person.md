# Person Schema

```txt
Person#/extends
```

A schema representing a person involved in an incident, detailing their personal information, involvement, and condition.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | Yes        | Unknown status | No           | Forbidden         | Allowed               | none                | [Minor.json\*](../schemas/Minor.json "open original schema") |

## extends Type

`object` ([Person](minor-person.md))

# extends Properties

| Property                                 | Type      | Required | Nullable       | Defined by                                                                              |
| :--------------------------------------- | :-------- | :------- | :------------- | :-------------------------------------------------------------------------------------- |
| [involvement](#involvement)              | `string`  | Required | cannot be null | [Person](person-properties-involvement.md "Person#/properties/involvement")             |
| [first\_name](#first_name)               | `string`  | Required | cannot be null | [Person](person-properties-first_name.md "Person#/properties/first_name")               |
| [middle\_name](#middle_name)             | `string`  | Required | cannot be null | [Person](person-properties-middle_name.md "Person#/properties/middle_name")             |
| [last\_name](#last_name)                 | `string`  | Required | cannot be null | [Person](person-properties-last_name.md "Person#/properties/last_name")                 |
| [address](#address)                      | `string`  | Required | cannot be null | [Person](person-properties-address.md "Person#/properties/address")                     |
| [gender](#gender)                        | `string`  | Required | cannot be null | [Person](person-properties-gender.md "Person#/properties/gender")                       |
| [license\_number](#license_number)       | `string`  | Optional | cannot be null | [Person](person-properties-license_number.md "Person#/properties/license_number")       |
| [age](#age)                              | `integer` | Required | cannot be null | [Person](person-properties-age.md "Person#/properties/age")                             |
| [driver\_error](#driver_error)           | `string`  | Optional | cannot be null | [Person](person-properties-driver_error.md "Person#/properties/driver_error")           |
| [injury](#injury)                        | `string`  | Required | cannot be null | [Person](person-properties-injury.md "Person#/properties/injury")                       |
| [alcohol\_suspicion](#alcohol_suspicion) | `boolean` | Required | cannot be null | [Person](person-properties-alcohol_suspicion.md "Person#/properties/alcohol_suspicion") |
| [drugs\_suspicion](#drugs_suspicion)     | `boolean` | Required | cannot be null | [Person](person-properties-drugs_suspicion.md "Person#/properties/drugs_suspicion")     |
| [seatbelt\_state](#seatbelt_state)       | `string`  | Optional | cannot be null | [Person](person-properties-seatbelt_state.md "Person#/properties/seatbelt_state")       |
| [helmet\_state](#helmet_state)           | `string`  | Optional | cannot be null | [Person](person-properties-helmet_state.md "Person#/properties/helmet_state")           |
| [hospital](#hospital)                    | `string`  | Required | cannot be null | [Person](person-properties-hospital.md "Person#/properties/hospital")                   |

## involvement

Defines the roles a person can have in an incident.

`involvement`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-involvement.md "Person#/properties/involvement")

### involvement Type

`string`

### involvement Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value          | Explanation |
| :------------- | :---------- |
| `"Pedestrian"` |             |
| `"Witness"`    |             |
| `"Passenger"`  |             |
| `"Driver"`     |             |

## first\_name

The first name of the person.

`first_name`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-first_name.md "Person#/properties/first_name")

### first\_name Type

`string`

## middle\_name

The middle name of the person.

`middle_name`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-middle_name.md "Person#/properties/middle_name")

### middle\_name Type

`string`

## last\_name

The last name of the person.

`last_name`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-last_name.md "Person#/properties/last_name")

### last\_name Type

`string`

## address

The residential address of the person.

`address`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-address.md "Person#/properties/address")

### address Type

`string`

## gender

Enumerates the gender identities.

`gender`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-gender.md "Person#/properties/gender")

### gender Type

`string`

### gender Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value      | Explanation |
| :--------- | :---------- |
| `"Male"`   |             |
| `"Female"` |             |
| `"Other"`  |             |

## license\_number

The driver's license number if applicable.

`license_number`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-license_number.md "Person#/properties/license_number")

### license\_number Type

`string`

## age

The age of the person.

`age`

*   is required

*   Type: `integer`

*   cannot be null

*   defined in: [Person](person-properties-age.md "Person#/properties/age")

### age Type

`integer`

## driver\_error

Lists possible driver errors contributing to incidents.

`driver_error`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-driver_error.md "Person#/properties/driver_error")

### driver\_error Type

`string`

### driver\_error Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Fatigued Or Asleep"` |             |
| `"Inattentive"`        |             |
| `"Too Fast"`           |             |
| `"Too Close"`          |             |
| `"No Signal"`          |             |
| `"Bad Overtaking"`     |             |
| `"Bad Turning"`        |             |
| `"Using Cellphone"`    |             |

## injury

Categorizes the severity of injuries.

`injury`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-injury.md "Person#/properties/injury")

### injury Type

`string`

### injury Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value           | Explanation |
| :-------------- | :---------- |
| `"Fatal"`       |             |
| `"Serious"`     |             |
| `"Minor"`       |             |
| `"Not Injured"` |             |

## alcohol\_suspicion

Indicates suspicion of alcohol influence.

`alcohol_suspicion`

*   is required

*   Type: `boolean`

*   cannot be null

*   defined in: [Person](person-properties-alcohol_suspicion.md "Person#/properties/alcohol_suspicion")

### alcohol\_suspicion Type

`boolean`

## drugs\_suspicion

Indicates suspicion of drug influence.

`drugs_suspicion`

*   is required

*   Type: `boolean`

*   cannot be null

*   defined in: [Person](person-properties-drugs_suspicion.md "Person#/properties/drugs_suspicion")

### drugs\_suspicion Type

`boolean`

## seatbelt\_state

Describes the correctness of safety gear usage.

`seatbelt_state`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-seatbelt_state.md "Person#/properties/seatbelt_state")

### seatbelt\_state Type

`string`

### seatbelt\_state Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Worn Correctly"`     |             |
| `"Not Worn"`           |             |
| `"Not Worn Correctly"` |             |

## helmet\_state

Describes the correctness of safety gear usage.

`helmet_state`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-helmet_state.md "Person#/properties/helmet_state")

### helmet\_state Type

`string`

### helmet\_state Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Worn Correctly"`     |             |
| `"Not Worn"`           |             |
| `"Not Worn Correctly"` |             |

## hospital

The name of the hospital the person was taken to, if any.

`hospital`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Person](person-properties-hospital.md "Person#/properties/hospital")

### hospital Type

`string`

# Person Definitions

## Definitions group InvolvementEnum

Reference this group by using

```json
{"$ref":"Person#/definitions/InvolvementEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group GenderEnum

Reference this group by using

```json
{"$ref":"Person#/definitions/GenderEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group DriverErrorEnum

Reference this group by using

```json
{"$ref":"Person#/definitions/DriverErrorEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group InjuryEnum

Reference this group by using

```json
{"$ref":"Person#/definitions/InjuryEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |

## Definitions group WearStateEnum

Reference this group by using

```json
{"$ref":"Person#/definitions/WearStateEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |
