# Untitled string in Person Schema

```txt
Person#/definitions/InvolvementEnum
```

Defines the roles a person can have in an incident.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                     |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Person.json\*](../schemas/Person.json "open original schema") |

## InvolvementEnum Type

`string`

## InvolvementEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value          | Explanation |
| :------------- | :---------- |
| `"Pedestrian"` |             |
| `"Witness"`    |             |
| `"Passenger"`  |             |
| `"Driver"`     |             |
