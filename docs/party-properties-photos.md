# Untitled array in Party Schema

```txt
Party#/properties/photos
```

An array of photos related to the party.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Party.json\*](../schemas/Party.json "open original schema") |

## photos Type

`object[]` ([Photo](photo.md))
