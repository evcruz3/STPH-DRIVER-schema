# Untitled array in Party Schema

```txt
Party#/properties/minors
```

An array of minors in a party.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                   |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Party.json\*](../schemas/Party.json "open original schema") |

## minors Type

unknown\[] ([Minor](minor.md))
