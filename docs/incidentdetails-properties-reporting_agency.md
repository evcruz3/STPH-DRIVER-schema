# Untitled string in Incident Details Schema

```txt
IncidentDetails#/properties/reporting_agency
```

The agencies that can report an incident.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## reporting\_agency Type

`string`

## reporting\_agency Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"PNP"`                |             |
| `"Local Traffic Unit"` |             |
| `"BGCEA"`              |             |
| `"CCTO"`               |             |
| `"MMDA Metrobase"`     |             |
| `"Other"`              |             |
