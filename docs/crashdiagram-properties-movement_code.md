# Untitled string in Crash Diagram Schema

```txt
CrashDiagram#/properties/movement_code
```



| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                 |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [CrashDiagram.json\*](../schemas/CrashDiagram.json "open original schema") |

## movement\_code Type

`string`
