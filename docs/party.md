# Party Schema

```txt
Party
```

A representation of a party including adults, minors, a vehicle, and photos.

| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                 |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | No           | Forbidden         | Forbidden             | none                | [Party.json](../schemas/Party.json "open original schema") |

## Party Type

`object` ([Party](party.md))

# Party Properties

| Property            | Type     | Required | Nullable       | Defined by                                                     |
| :------------------ | :------- | :------- | :------------- | :------------------------------------------------------------- |
| [name](#name)       | `string` | Optional | cannot be null | [Party](party-properties-name.md "Party#/properties/name")     |
| [adults](#adults)   | `array`  | Optional | cannot be null | [Party](party-properties-adults.md "Party#/properties/adults") |
| [minors](#minors)   | `array`  | Optional | cannot be null | [Party](party-properties-minors.md "Party#/properties/minors") |
| [vehicle](#vehicle) | `object` | Optional | cannot be null | [Party](vehicle.md "Vehicle#/properties/vehicle")              |
| [photos](#photos)   | `array`  | Optional | cannot be null | [Party](party-properties-photos.md "Party#/properties/photos") |

## name

A label or name for the party involved in the incident

`name`

*   is optional

*   Type: `string`

*   cannot be null

*   defined in: [Party](party-properties-name.md "Party#/properties/name")

### name Type

`string`

## adults

An array of adults in a party.

`adults`

*   is optional

*   Type: `object[]` ([Person](person.md))

*   cannot be null

*   defined in: [Party](party-properties-adults.md "Party#/properties/adults")

### adults Type

`object[]` ([Person](person.md))

## minors

An array of minors in a party.

`minors`

*   is optional

*   Type: unknown\[] ([Minor](minor.md))

*   cannot be null

*   defined in: [Party](party-properties-minors.md "Party#/properties/minors")

### minors Type

unknown\[] ([Minor](minor.md))

## vehicle

The vehicle with the party.

`vehicle`

*   is optional

*   Type: `object` ([Vehicle](vehicle.md))

*   cannot be null

*   defined in: [Party](vehicle.md "Vehicle#/properties/vehicle")

### vehicle Type

`object` ([Vehicle](vehicle.md))

## photos

An array of photos related to the party.

`photos`

*   is optional

*   Type: `object[]` ([Photo](photo.md))

*   cannot be null

*   defined in: [Party](party-properties-photos.md "Party#/properties/photos")

### photos Type

`object[]` ([Photo](photo.md))
