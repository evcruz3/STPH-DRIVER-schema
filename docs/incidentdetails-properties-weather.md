# Untitled string in Incident Details Schema

```txt
IncidentDetails#/properties/weather
```

The types of weather conditions at the time of the incident.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## weather Type

`string`

## weather Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                      | Explanation |
| :------------------------- | :---------- |
| `"Day"`                    |             |
| `"Clear Night"`            |             |
| `"Cloudy"`                 |             |
| `"Fog"`                    |             |
| `"Hail"`                   |             |
| `"Partially Cloudy Day"`   |             |
| `"Partially Cloudy Night"` |             |
| `"Rain"`                   |             |
| `"Thunderstorm"`           |             |
| `"Wind"`                   |             |
