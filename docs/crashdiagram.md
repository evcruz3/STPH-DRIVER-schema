# Crash Diagram Schema

```txt
CrashDiagram
```



| Abstract            | Extensible | Status         | Identifiable | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                               |
| :------------------ | :--------- | :------------- | :----------- | :---------------- | :-------------------- | :------------------ | :----------------------------------------------------------------------- |
| Can be instantiated | Yes        | Unknown status | No           | Forbidden         | Allowed               | none                | [CrashDiagram.json](../schemas/CrashDiagram.json "open original schema") |

## Crash Diagram Type

`object` ([Crash Diagram](crashdiagram.md))

# Crash Diagram Properties

| Property                         | Type     | Required | Nullable       | Defined by                                                                                         |
| :------------------------------- | :------- | :------- | :------------- | :------------------------------------------------------------------------------------------------- |
| [crash\_type](#crash_type)       | `string` | Required | cannot be null | [Crash Diagram](crashdiagram-properties-crash_type.md "CrashDiagram#/properties/crash_type")       |
| [movement\_code](#movement_code) | `string` | Required | cannot be null | [Crash Diagram](crashdiagram-properties-movement_code.md "CrashDiagram#/properties/movement_code") |

## crash\_type



`crash_type`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Crash Diagram](crashdiagram-properties-crash_type.md "CrashDiagram#/properties/crash_type")

### crash\_type Type

`string`

### crash\_type Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                                                 | Explanation |
| :---------------------------------------------------- | :---------- |
| `"Pedestrian On Foot"`                                |             |
| `"Vehicle From Adjacent Direction Intersection Only"` |             |
| `"Vehicle From Adjacent Direction"`                   |             |
| `"Vehicle From Opposing Direction"`                   |             |
| `"Vehicle From Same Direction"`                       |             |
| `"Maneuvering"`                                       |             |
| `"Overtaking"`                                        |             |
| `"On Path"`                                           |             |
| `"Off Path Straight"`                                 |             |
| `"Off Path Curve"`                                    |             |
| `"Passenger And Miscellaneous"`                       |             |

## movement\_code



`movement_code`

*   is required

*   Type: `string`

*   cannot be null

*   defined in: [Crash Diagram](crashdiagram-properties-movement_code.md "CrashDiagram#/properties/movement_code")

### movement\_code Type

`string`

# Crash Diagram Definitions

## Definitions group CrashTypeEnum

Reference this group by using

```json
{"$ref":"CrashDiagram#/definitions/CrashTypeEnum"}
```

| Property | Type | Required | Nullable | Defined by |
| :------- | :--- | :------- | :------- | :--------- |
