# Untitled string in Vehicle Schema

```txt
Vehicle#/definitions/VehicleClassificationEnum
```

The classification categories for vehicles.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Vehicle.json\*](../schemas/Vehicle.json "open original schema") |

## VehicleClassificationEnum Type

`string`

## VehicleClassificationEnum Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                  | Explanation |
| :--------------------- | :---------- |
| `"Private"`            |             |
| `"Government"`         |             |
| `"Public Or For Hire"` |             |
| `"Diplomat"`           |             |
