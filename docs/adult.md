# Adult Schema

```txt
Adult
```

An adult involved in an incident, detailing their personal information, involvement, and condition.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                 |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :--------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [Adult.json](../schemas/Adult.json "open original schema") |

## Adult Type

unknown ([Adult](adult.md))
