# Untitled string in Incident Details Schema

```txt
IncidentDetails#/properties/collision_type
```

Different types of collisions that can occur.

| Abstract            | Extensible | Status         | Identifiable            | Custom Properties | Additional Properties | Access Restrictions | Defined In                                                                       |
| :------------------ | :--------- | :------------- | :---------------------- | :---------------- | :-------------------- | :------------------ | :------------------------------------------------------------------------------- |
| Can be instantiated | No         | Unknown status | Unknown identifiability | Forbidden         | Allowed               | none                | [IncidentDetails.json\*](../schemas/IncidentDetails.json "open original schema") |

## collision\_type Type

`string`

## collision\_type Constraints

**enum**: the value of this property must be equal to one of the following values:

| Value                   | Explanation |
| :---------------------- | :---------- |
| `"Head On"`             |             |
| `"Rear End"`            |             |
| `"Right Angle"`         |             |
| `"Other Angle"`         |             |
| `"Side Swipe"`          |             |
| `"Overturned Vehicle"`  |             |
| `"Hit Object On Road"`  |             |
| `"Hit Object Off Road"` |             |
| `"Hit Parked Vehicle"`  |             |
| `"Hit Pedestrian"`      |             |
| `"Hit Animal"`          |             |
| `"Other"`               |             |
