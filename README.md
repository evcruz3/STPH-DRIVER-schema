# PVD Data Model

JSON schemas for the STPH DRIVER module

## Visualization

For the visualization of latest release of the json-schema, you may access the [STPH DRIVER Data Model Visualizer](https://stph-driver-schema-evcruz3-4fbafdfa2765c29964e241f2b138f7529762.gitlab.io/)

## Documentation

The markdown documentation for the json-schema may be read [here](./docs/README.md)
